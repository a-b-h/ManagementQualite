# Interface graphique de l'outil de hashing
import PySimpleGUI as sg
import function

#
callfunc=function.hash()

# Disposition des pages des onglets
layout1 = [[sg.Text('Algorithme', size=(10,1)),sg.Combo(values=('sha1', 'sha256', 'sha512', 'md5', 'blake2b'), default_value='sha256', size=(12,3), key='kAlgo')],
           [sg.Text('Choisir un fichier', size=(14,1)),sg.FileBrowse()],
           [sg.Checkbox('Utilisation de Sel', default=True, key='kSel')],
           [sg.Button('Chiffrer')]]
layout2 = [[sg.Text('Choisir un fichier', size=(14,1)),sg.FileBrowse()],
           [sg.Button('Déchiffrer')]]
layout3 = [[sg.Text('Algorithme', size=(10,1)),sg.Combo(values=('sha1', 'sha256', 'sha512', 'md5', 'blake2b'), default_value='sha256', size=(12,3), key='kAlgo')],
           [sg.Text('Choisir un fichier', size=(14,1)),sg.FileBrowse()],
           [sg.Checkbox('Utilisation de Sel', default=True, key='kSel')],
           [sg.Button('Hasher')]]

# Définition des onglets  
tabgrp = [[sg.TabGroup([[sg.Tab('Chiffrer', layout1, title_color='black',border_width =10, background_color='gray', element_justification= 'center'),
                         sg.Tab('Déchiffrer', layout2,title_color='black',background_color='gray', element_justification='center'),
                         sg.Tab('Hasher', layout3,title_color='black',background_color='gray', element_justification='center')]],
                         tab_location='centertop', title_color='black', tab_background_color='gray',selected_title_color='Black', selected_background_color='white', border_width=5),
                         sg.Button('Close')]]

# Définition de la fenêtre
window =sg.Window("Hashing Tool",tabgrp, element_justification='center')

# Lecture des valeurs entrées
event,values=window.read()

# Action des boutons
# Action de chiffrer
if event == 'Chiffrer':
    # Génération de la clé de chiffrage
    callfunc.generateKey()
    # Génération du sel à partir de l'ID (initialisé à 1 tant que non lié à la BDD)
    callfunc.generatesalt()
    # Chiffrement du fichier sélectionné
    callfunc.cypher(values['Browse'])
    # Hash du fichier chiffré
    callfunc.Hashfile(values['Browse'], values['kAlgo'], values['kSel'])
elif event == 'Déchiffrer':
    # Déchiffrement du fichier sélectionné
    callfunc.decypher(values['Browse1'],'unlock.key')
elif event == 'Hasher':
    # Génération du sel à partir de l'ID (initialisé à 1 tant que non lié à la BDD) 
    callfunc.generatesalt()
    # Hash du fichier sélectionné
    callfunc.Hashfile(values['Browse4'], values['kAlgo'], values['kSel'])

window.close()
