### Hashing project ###
from ast import And
import hashlib
import random
from cryptography.fernet import Fernet


class hash:

    def __init__(self):                             #constructeur
        self.password=""
        self.name=""
        self.key=""
        self.encryptonSuite=""
        self.decryptionSuite=""
        self.firstname=""
        self.hashword=""
        self.word=""
        self.sel=""
        self.id="1"
        self.hashtab=[]

    def generatesalt(self):                                 #Genérateur de sel
        self.sel="ALEXANDREBAPTISTEROGER"+ self.id          #création du sel en utilisant les prénoms des créateurs + l'id de la base de données correspondant a l'utilisateur
        lenSel=random.getrandbits(256-len(self.sel))        
        self.sel=self.sel+str(lenSel)                       #complétion du sel pour qu'il fasse 256bits
        print(self.sel)

    def generateKey(self):                                  # Générateur de clés AES
        self.key=Fernet.generate_key()
        with open('unlock.key','wb') as unlock:             # écriture dans un fichier mais a long terme doit être stockée sur la base de donnée
            unlock.write(self.key)

    def readkey(self):                                      #Ouvre le fichier ou la clef au dessus
        with open('unlock.key','r') as unlock:              # mais a long terme intéraction avec la BDD
            reader=unlock.read()
        print('Key :',reader)

### FONCTION DE HASHAGE APPELANT LA LIBRAIRIE HASHLIB ###
    def SHA1(self,word):
        return hashlib.sha1(word)

    def SHA256(self,word):
        return hashlib.sha256(word)

    def SHA512(self,word):
        return hashlib.sha512(word)

    def MD5(self,word):
        return hashlib.md5(word)

    def Blake2B(self,word):
        return hashlib.blake2b(word)
    
    def saltHash(self,halgo,word,salt):
        return hashlib.pbkdf2_hmac(halgo,word,salt.encode(),1)

###----------------------------------------------------------------###

    def Hashfile(self,namefile,halgo,sel):                          #fonction de hashage du fichier appelant les fonctions du dessus en fonction des besoin de l'utilisateur et le stocke.
        
        with open(namefile,'rb') as hasher:
            if halgo=="sha1" and sel==False:
                self.hashtab.append(self.SHA1(hasher.read()))

            elif halgo=="sha256" and sel==False:
                self.hashtab.append(self.SHA256(hasher.read()))

            elif halgo=="sha512" and sel==False:
                self.hashtab.append(self.SHA512(hasher.read()))

            elif halgo=="md5" and sel==False:
                self.hashtab.append(self.MD5(hasher.read()))

            elif halgo=="blake2b" and sel==False:
                self.hashtab.append(self.Blake2B(hasher.read()))

            elif halgo and sel==True:
                self.hashtab.append(self.saltHash(halgo,hasher.read(),self.sel))

            else:
                print("Error no hashing algorithm")

        with open(namefile+"hashed.txt",'a') as hasher:
            for line in self.hashtab:
                if halgo and sel==True:
                    hasher.write(str(line.hex()+"\n"))
                else:
                    hasher.write(str(line.hexdigest())+"\n")
     
    
    def cypher(self,namefile):                                      #Fonction qui chiffre grace à une clés de chiffrement un fichier dans un nouveau fichier.
        self.encryptonSuite=Fernet(self.key)
        with open(namefile,"rb") as readfile:
            cypherText=readfile.read()
        with open(namefile+"encrypted.txt",'wb') as cypher:
            cypherText=self.encryptonSuite.encrypt(cypherText)
            cypher.write(cypherText)

    def decypher(self,namefile,key):                                #Fonction qui déchiffre grace à la clés de chiffrement.
        with open(key,'rb') as bkey:
            self.key=bkey.read()
        self.encryptonSuite=Fernet(self.key)
        with open(namefile,'rb') as decyph:
            print(self.encryptonSuite.decrypt(decyph.read()))

### Ce code est encore en développement il n'est pas totalement opérationnel et possède des points à améliorer et à optimiser surtout dans le domaine de stockage des clés (une base de données).
