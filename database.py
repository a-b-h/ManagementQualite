import mysql.connector 

class database():

    def __init__(self):
        pass

    def readHash(self, vHash):
        # Connexion à la base de données de l'outil
        conn = mysql.connector.connect(host="localhost",user="admin",password="admin",database="hashbdd",pool_name='batman',pool_size = 3)
        cursor = conn.cursor()
        conn.close()
        # Requête SQL à exécuter : Recherche des informations concernant un certain hash (pour notamment récupérer la clé de chiffrement)
        cursor.execute("""SELECT * FROM fichier WHERE hash = %s""", vHash)
        rows = cursor.fetchall()
        # Retour de l'information
        return (rows)

    def readMax(self):
        # Connexion à la base de données de l'outil
        conn = mysql.connector.connect(host="localhost",user="admin",password="admin",database="hashbdd",pool_name='batman',pool_size = 3)
        cursor = conn.cursor()
        conn.close()
        # Requête SQL à exécuter : Identification du dernier id (utilisé pour créer le sel)
        cursor.execute("""SELECT MAX(idSel) FROM fichier""")
        rows = cursor.fetchall()
        return (rows[0][0])

    def insert(self, vKey, vSel, vHash):
        # Connexion à la base de données de l'outil
        conn = mysql.connector.connect(host="localhost",user="admin",password="admin",database="hashbdd",pool_name='batman',pool_size = 3)
        cursor = conn.cursor()
        conn.close()
        # Requête SQL à exécuter : Ajout des informations sur un fichier (clé de déchiffrement, sel, hash)
        cursor.execute("""INSERT INTO fichier (statut, clef, sel, hash) VALUES (True, %s, %s, %s)""", vKey, vSel, vHash)

