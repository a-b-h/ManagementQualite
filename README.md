<h1><center>Réalisation d’une application graphique de hashage et crypto symétrique</center><h1>
  
<h2>VERSION 1.0.0<h2>

La fiabilité de notre code dépend de la qualité et des procédures mises en place pour développer cette application.
L'objectif étant d'éviter les inconnus tout en fournissant une application fiable et robuste à tous nos utilisateurs.
Pour se faire nous avons des indicateurs mis en place pour vérifier fréquemment notre application.
Des points sont fait régulièrement avec l'équipe pour évaluer l'avancement du projet.

Voici un schéma de notre application.

![Schéma](/image/sch%C3%A9ma.PNG)

Dans cette application, nous proposons : 
- un ensemble de fonction de hashage (sha1,sha256 ...) 
- un outil de transformation AES 
- et la transformation de fichier (chiffrement/déchiffrement)
                                    
                                          

Les développeurs sont : 
- Baptiste, chef de projet 
- Roger, développeur 
- Alexandre, développeur


REQUIREMENT
 - Base de données (notre base de donnée est sur une VM, les test seront fait en amont par l'admin du GITLAB)
 - IDE (Visual Code Studio)


GUIDE ET STANDARDISATION
Les APIs utilisées dans ce projet sont : 
 - SonarCloud (magnification de code) 
 - PySimpleGUI (interface Graphique) 4.60.1
 - MariaDB 10.6.7 database server
 - Python 3.10.4
 - Adapté la taille de l'écran
 - Mise à jour des APIs et autres logiciels tous les 6 mois sauf en cas de faille de sécurité (notre infrastructure est une infra de type bac à sable)

LIBRAIRIE
 - hashlib 3.10.4
 - cryptography 37.0.2
 - mysqlconnector 5.1


<h2>Toute demande pour rejoindre notre équipe de développement sera examiné par notre ADMINISTRATEUR<h2>

Mail de contact: 
 - mail: dabouniet@devtropfort.com
 - téléphone: 07 63 21 02 32

>Pour modifier ou essayer le code, il suffit d'importer le projet git,puis demander les droits pour développer puis se créer son espace dans BRANCHE (chacun des participants a son espace pour coder, et tout est remis en commun dans le main).


CONNECTION AU GIT
 - CTRL + SHIFT + P puis marquer gitclone.
 - Se connecter à son compte git.
 - Puis cloner son git sur son IDE.

 >Il faudra ensuite créer un Token sur votre compte git pour pouvoir avoir accès au gitlab via votre IDE.  
 >Il suffit de la rubrique EDIT PROFILE > Acess Token. ![accesstoken](/image/token.png)  
> Puis créer un token (Le token n'apparatra que lorsque que vous allez créer le token, on ne pourra plus le voir après).  
> Ajouter ce token quand votre IDE vous le demande.  
> Il faudra sûrement changer les fichiers de conf de votre IDE pour avoir accès à votre compte GIT.

exemple: sur VSCODE il faudra renseigner le USERNAME et l'email pour se connecter.

 - git config --global user.name="Jean-Michel"
 - git config --global user.email="Jean-Michel@coucoucestmoi.fr"

<h1>Avancement du projet<h1>

<h3>En cours</h3>

- [x] Architecture du logiciel
- [x] Répartition des tâches
- [x] Codage des fonctions

<h3>Ready</h3>

- [x] Commentaire des fonctions
- [x] Révision des fonctions par un autre développeur
- [x] Push du code sur la branche dev

<h3>Done</h3> 

- [x] Validée par le chef de projet
- [x] Présentation fonctionnelle
- [x] Merge sur la branche MAIN

| _TO DO LIST_ | ![validé](/image/valid%C3%A9.jpg)/![encours](/image/encours.jpg)/![croix](/image/croix.jpg) | Avance de la tâche |
| :---:   | :-: | :---: |
| Création de la base de donnée | ![validé](/image/valid%C3%A9.jpg) | 100% |
| Création des fonction de chiffrement | ![validé](/image/valid%C3%A9.jpg) | 100% |
| Création de l'interface graphique | ![validé](/image/valid%C3%A9.jpg) | 100% |
| Edition du readme  | ![validé](/image/valid%C3%A9.jpg) | 100% |
| Pipeline SonarCloud | ![validé](/image/valid%C3%A9.jpg) | 100% |
| Lien vers la BDD | ![encours](/image/encours.jpg) | 50% |

 
<h2>Procédure d'utilisation de notre application<h2>
 
Voici le schéma d'utilisation de notre application

[![Schéma d'utilisation du logiciel](https://mermaid.ink/img/pako:eNptkTuOgzAURbdiuSJSUgSJKShG4huSaCpmqpDCGBOsmI-MKUaEBbEONjYGHMlIAw333GMZvddDXGcE2vDBUVOAbz-pgHyc24-gjLZIkI7fweHwCVzDaRpGMRK0rnar5i7Ny2Mkf3m9V9A854QPennpI4QL9CCKeorGhIG6A1VdqeKyFL7UW033FxoYceQcdzoKZ2RaHxt4mqF1NDcwMr58a0POhssQfpqpotfl0mlUdwaLdF1DqIeTHiI9nN9hfmeAbiHFBSUcrGOZxrsqFjP9Z5qpNk3c-9OI1Un1X3APS8JLRDO5r35mCRQFKUkCbfmZIf5MYFIN0uuaTK4uyKioObRzxFqyh6gTdfxbYWgL3pG35FMkd18qa_gDINqaZw)](https://mermaid.live/edit#pako:eNptkTuOgzAURbdiuSJSUgSJKShG4huSaCpmqpDCGBOsmI-MKUaEBbEONjYGHMlIAw333GMZvddDXGcE2vDBUVOAbz-pgHyc24-gjLZIkI7fweHwCVzDaRpGMRK0rnar5i7Ny2Mkf3m9V9A854QPennpI4QL9CCKeorGhIG6A1VdqeKyFL7UW033FxoYceQcdzoKZ2RaHxt4mqF1NDcwMr58a0POhssQfpqpotfl0mlUdwaLdF1DqIeTHiI9nN9hfmeAbiHFBSUcrGOZxrsqFjP9Z5qpNk3c-9OI1Un1X3APS8JLRDO5r35mCRQFKUkCbfmZIf5MYFIN0uuaTK4uyKioObRzxFqyh6gTdfxbYWgL3pG35FMkd18qa_gDINqaZw)


<h2>Prochaine Features 2.0.0<h2>

Pour les prochaines versions:
 - Amélioration d'interface graphique
 - Support d'autres méthodes de hashage/chiffrement
